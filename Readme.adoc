= decimal to roman (rust)

My recent implementation of the well known roman numeral kata in Rust in order to learn Rust.

== Release build optimizations

We have optimized the release build size in link:Cargo.toml[]:

[source,toml]
----
include::Cargo.toml[tags=profile.release]
----

== Download linux binary

Download the https://gitlab.com/tpraxl-learns/rust/decimal-to-roman/-/jobs/artifacts/main/raw/target/release/decimal-to-roman?job=release%3Aoptimized&inline=false[latest linux release binary].
