use std::io::BufRead;
use std::num::NonZeroU32;

use input_error::InputError;

pub mod input_error;

type Integer = u32;
type NonZeroInteger = NonZeroU32;

pub fn read_decimal_input_or_fail<R>(reader: R) -> Result<Integer, InputError>
where
    R: BufRead,
{
    read_input(reader).and_then(string_to_non_zero_integer)
}

fn read_input<R>(mut reader: R) -> Result<String, InputError>
where
    R: BufRead,
{
    let mut input = String::new();
    reader.read_line(&mut input)?;
    Ok(input)
}

fn string_to_non_zero_integer(str: String) -> Result<Integer, InputError> {
    Ok(Integer::from(str.trim().parse::<NonZeroInteger>()?))
}

#[cfg(test)]
mod test_read_decimal_input {
    use std::io;
    use std::io::{BufReader, ErrorKind, Read};

    use super::input_error::InputError;
    use super::read_decimal_input_or_fail;

    struct AlwaysFailingInput {}

    impl Read for AlwaysFailingInput {
        fn read(&mut self, _buf: &mut [u8]) -> Result<usize, io::Error> {
            Err(io::Error::from(ErrorKind::Other))
        }
    }

    #[test]
    fn fails_if_read_line_triggers_io_error() {
        let result = read_decimal_input_or_fail(BufReader::new(AlwaysFailingInput {}));
        assert!(result.is_err());
        match result.unwrap_err() {
            InputError::Io(_) => assert!(true),
            _ => assert!(false),
        }
    }

    #[test]
    fn fails_for_empty_input() {
        let input = b"";
        let result = read_decimal_input_or_fail(&input[..]);
        assert!(result.is_err());
        match result.unwrap_err() {
            InputError::ParseInt(_) => assert!(true), // todo improve once int_error_matching is stable
            _ => assert!(false),
        }
    }

    #[test]
    fn returns_decimal_for_valid_input() {
        let expected = 104;
        let input = expected.to_string().into_bytes();
        assert_eq!(104, read_decimal_input_or_fail(&input[..]).unwrap());
    }
}

#[cfg(test)]
mod test_conversion_string_to_decimal {
    use super::*;

    #[test]
    fn converts_valid_number_1() {
        assert_eq!(1, string_to_non_zero_integer(String::from("1")).unwrap());
    }

    #[test]
    fn converts_valid_number_max() {
        assert_eq!(
            Integer::MAX,
            string_to_non_zero_integer(Integer::MAX.to_string()).unwrap()
        );
    }

    #[test]
    fn fails_for_invalid_number_0() {
        let result = string_to_non_zero_integer(String::from("0"));
        assert!(result.is_err());
        match result.unwrap_err() {
            InputError::ParseInt(_) => assert!(true), // todo improve once int_error_matching is stable
            _ => assert!(false),
        }
    }

    #[test]
    fn fails_for_negative_decimal() {
        let result = string_to_non_zero_integer(String::from("-1"));
        assert!(result.is_err());
        match result.unwrap_err() {
            InputError::ParseInt(_) => assert!(true), // todo improve once int_error_matching is stable
            _ => assert!(false),
        }
    }

    #[test]
    fn fails_for_more_than_max() {
        let max = Integer::from(Integer::MAX);
        let more_than_max = max.to_string() + "0";
        let result = string_to_non_zero_integer(more_than_max);
        assert!(result.is_err());
        match result.unwrap_err() {
            InputError::ParseInt(_) => assert!(true), // todo improve once int_error_matching is stable
            _ => assert!(false),
        }
    }

    #[test]
    fn fails_for_empty_string() {
        let result = string_to_non_zero_integer(String::from(""));
        assert!(result.is_err());
        match result.unwrap_err() {
            InputError::ParseInt(_) => assert!(true), // todo improve once int_error_matching is stable
            _ => assert!(false),
        }
    }
}
