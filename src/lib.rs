type Integer = u32;

pub trait ConvertibleToRoman {
    fn to_roman(&self) -> String;
}

impl ConvertibleToRoman for Integer {
    fn to_roman(&self) -> String {
        decimal_to_roman(*self)
    }
}

#[cfg(test)]
mod test_trait {
    use super::ConvertibleToRoman;

    #[test]
    fn can_be_used_on_u32() {
        assert_eq!(1.to_roman(), "I")
    }
}

struct RomanUnit<'a>(Integer, &'a str);

pub fn decimal_to_roman(dec: Integer) -> String {
    iterate_units(
        dec,
        &[
            RomanUnit(1000, "M"),
            RomanUnit(900, "CM"),
            RomanUnit(500, "D"),
            RomanUnit(400, "CD"),
            RomanUnit(100, "C"),
            RomanUnit(90, "XC"),
            RomanUnit(50, "L"),
            RomanUnit(40, "XL"),
            RomanUnit(10, "X"),
            RomanUnit(9, "IX"),
            RomanUnit(5, "V"),
            RomanUnit(4, "IV"),
            RomanUnit(1, "I"),
        ],
    )
}

fn iterate_units(dec: Integer, units: &[RomanUnit]) -> String {
    let mut rest = dec;
    let mut result = String::new();
    for unit in units.iter() {
        let (roman_string, local_rest): (String, Integer) = apply_unit(rest, unit);
        rest = local_rest;
        result.push_str(&roman_string);
    }
    result
}

fn apply_unit(
    decimal: Integer,
    RomanUnit(unit_decimal, unit_literal): &RomanUnit,
) -> (String, Integer) {
    let mut result = String::new();

    let times = decimal / unit_decimal;
    result.push_str(&render_roman_literal(unit_literal, times));

    let rest = decimal % unit_decimal;

    (result, rest)
}

fn render_roman_literal(roman_literal: &str, times: Integer) -> String {
    let mut roman = String::new();
    for _ in 0..times {
        roman.push_str(roman_literal);
    }
    roman
}

#[cfg(test)]
mod test_decimal_to_roman_conversion {
    use super::decimal_to_roman;

    #[test]
    fn converts_1_to_i() {
        assert_eq!(decimal_to_roman(1), "I")
    }

    #[test]
    fn converts_2_to_ii() {
        assert_eq!(decimal_to_roman(2), "II")
    }

    #[test]
    fn converts_3_to_iii() {
        assert_eq!(decimal_to_roman(3), "III")
    }

    #[test]
    fn converts_4_to_iv() {
        assert_eq!(decimal_to_roman(4), "IV")
    }

    #[test]
    fn converts_5_to_v() {
        assert_eq!(decimal_to_roman(5), "V")
    }

    #[test]
    fn converts_6_to_vi() {
        assert_eq!(decimal_to_roman(6), "VI")
    }

    #[test]
    fn converts_7_to_vii() {
        assert_eq!(decimal_to_roman(7), "VII")
    }

    #[test]
    fn converts_8_to_viii() {
        assert_eq!(decimal_to_roman(8), "VIII")
    }

    #[test]
    fn converts_9_to_ix() {
        assert_eq!(decimal_to_roman(9), "IX")
    }

    #[test]
    fn converts_10_to_x() {
        assert_eq!(decimal_to_roman(10), "X")
    }

    #[test]
    fn converts_12_to_xii() {
        assert_eq!(decimal_to_roman(12), "XII")
    }

    #[test]
    fn converts_15_to_xv() {
        assert_eq!(decimal_to_roman(15), "XV")
    }

    #[test]
    fn converts_23_to_xxiii() {
        assert_eq!(decimal_to_roman(23), "XXIII")
    }

    #[test]
    fn converts_24_to_xxiv() {
        assert_eq!(decimal_to_roman(24), "XXIV")
    }

    #[test]
    fn converts_25_to_xxv() {
        assert_eq!(decimal_to_roman(25), "XXV")
    }

    #[test]
    fn converts_31_to_xxxi() {
        assert_eq!(decimal_to_roman(31), "XXXI")
    }

    #[test]
    fn converts_37_to_xxxvii() {
        assert_eq!(decimal_to_roman(37), "XXXVII")
    }

    #[test]
    fn converts_39_to_xxxix() {
        assert_eq!(decimal_to_roman(39), "XXXIX")
    }

    #[test]
    fn converts_43_to_xliii() {
        assert_eq!(decimal_to_roman(43), "XLIII")
    }

    #[test]
    fn converts_44_to_xliv() {
        assert_eq!(decimal_to_roman(44), "XLIV")
    }

    #[test]
    fn converts_50_to_l() {
        assert_eq!(decimal_to_roman(50), "L")
    }

    #[test]
    fn converts_98_to_xcviii() {
        assert_eq!(decimal_to_roman(98), "XCVIII")
    }

    #[test]
    fn converts_99_to_xcix() {
        assert_eq!(decimal_to_roman(99), "XCIX")
    }

    #[test]
    fn converts_100_to_c() {
        assert_eq!(decimal_to_roman(100), "C")
    }

    #[test]
    fn converts_1000_to_m() {
        assert_eq!(decimal_to_roman(1_000), "M")
    }

    #[test]
    fn converts_9999_to_mmmxxxvii() {
        assert_eq!(decimal_to_roman(3_037), "MMMXXXVII")
    }

    #[test]
    fn converts_3037_to_mmmxxxvii() {
        assert_eq!(decimal_to_roman(3_037), "MMMXXXVII")
    }

    #[test]
    fn converts_3999_to_mmmcmxcix() {
        assert_eq!(decimal_to_roman(3_999), "MMMCMXCIX")
    }
}
