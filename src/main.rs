use std::io::stdin;

use decimal_to_roman::ConvertibleToRoman;
use input::read_decimal_input_or_fail;

mod input;

fn main() {
    let decimal = read_decimal_input_or_fail(stdin().lock()).unwrap_or_else(|e| {
        eprintln!("{}", e);
        std::process::exit(1)
    });
    println!("{}", decimal.to_roman());
}
