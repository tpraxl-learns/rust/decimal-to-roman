use std::{fmt, io, num};

#[derive(Debug)]
pub enum InputError {
    Io(io::Error),
    ParseInt(num::ParseIntError),
}

impl fmt::Display for InputError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let err_msg = match self {
            InputError::Io(e) => e.to_string(),
            InputError::ParseInt(e) => e.to_string(),
        };
        write!(f, "Error: {}", err_msg)
    }
}

impl From<io::Error> for InputError {
    fn from(error: io::Error) -> Self {
        InputError::Io(error)
    }
}

impl From<num::ParseIntError> for InputError {
    fn from(error: num::ParseIntError) -> Self {
        InputError::ParseInt(error)
    }
}
